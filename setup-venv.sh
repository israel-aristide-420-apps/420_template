#!/bin/bash
echo "Creating venv..."
python3 -m venv .venv
echo "Venv created! To activate run ./.venv/bin/activate"